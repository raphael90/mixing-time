========================================================================
    Mixing time measurement algorithms
========================================================================

Implements a mixing time measurement algorithm using different algebra linear libraries: 
  -- Boost (uBlas)
  -- dlib
  -- Eigen (with Dense and Sparse matrix representation)

Make sure that a C++ compiler and boost library are installed on the system.
Makefiles are provided. For makefiles, compile the code with "make all".

/////////////////////////////////////////////////////////////////////////////
Parameters:

   -i:Input graph (tab separated list of edges) (default:'graph.txt')
 
/////////////////////////////////////////////////////////////////////////////
Usage:

Compute mixing time as:

mixing-time -i:graph.txt