#include <iostream>
#include <dlib/matrix.h>

#include <boost/numeric/ublas/matrix_sparse.hpp>

#include "stdafx.h"

#include <Eigen/Dense>
#include <Eigen/Sparse>

using Eigen::MatrixXd;
using Eigen::RowVectorXd;
using Eigen::SparseMatrix;
using Eigen::SparseVector;

using namespace std;
using namespace dlib;

namespace ublas = boost::numeric::ublas;

/**
 * @param  vec
 * @return
 */
double TotalDistanceVariation(Eigen::SparseMatrix<double> vec){
  double sum = 0;
  for (int  i = 0; i < vec.cols(); i++)
  {
    sum += abs(vec.coeff(0, i));
  }

  return sum/2;
}

/**
 * Measures the mixing time of a graph using
 * Eigen library's Sparse Matrix
 * @param  Graph   Undirected Graph
 * @param  epsilon 
 * @return         Mixing time
 */
double MixingTimeEigenSparse(PUNGraph Graph, double epsilon){
  const int n = Graph->GetNodes();
  SparseMatrix<double> P(n, n);
  SparseMatrix<double> ds(1, n);
  SparseMatrix<double> delta(1, n);

  for (TUNGraph::TEdgeI EI = Graph->BegEI(); EI < Graph->EndEI(); EI++) {
    const TUNGraph::TNodeI srcNI = Graph->GetNI(EI.GetSrcNId());
    const TUNGraph::TNodeI dstNI = Graph->GetNI(EI.GetDstNId());
    
    P.insert(srcNI.GetId(), dstNI.GetId()) = 1.0/(srcNI.GetOutDeg());
    P.insert(dstNI.GetId(), srcNI.GetId()) = 1.0/(dstNI.GetOutDeg());
  }

  // Stationary Distribution
  for (TUNGraph::TNodeI NI = Graph->BegNI(); NI < Graph->EndNI(); NI++) {
    ds.insert(0, NI.GetId()) = NI.GetOutDeg()/((double)2.0*Graph->GetEdges());
    delta.insert(0, NI.GetId()) = 1.0;
  }

  int t = 1;
  int t_2steps = 0;
  SparseMatrix<double> Pt, Pt_before, P0, Psqr;
  Pt = Pt_before = P0 = Psqr = P;
  while(t_2steps != 1){
      
      double maxi = 10;
      t_2steps = 1;
      Psqr = P;
      Pt_before = Pt;
      Pt = P0 * Psqr;
      double max_dist = 0;
      #pragma omp parallel for
      for (int i = 0; i < n; ++i)
      {
          double dist = TotalDistanceVariation((Pt.row(i) - ds));
          delta.coeffRef(0, i) = dist;
          
          if(delta.coeff(0, i) > max_dist){
              max_dist = delta.coeff(0, i);
          }
      }
      maxi = max_dist;
      while(maxi > epsilon){
          Psqr = Psqr * Psqr;
          t_2steps*=2;
          Pt_before = Pt;
          Pt = P0 * Psqr;
          double max_dist = 0;
          #pragma omp parallel for
          for (int i = 0; i < n; ++i)
          {
              double dist = TotalDistanceVariation((Pt.row(i) - ds));
              delta.coeffRef(0, i) = dist;
              
              if(delta.coeff(0, i) > max_dist){
                  max_dist = delta.coeff(0, i);
              }
          }
          maxi = max_dist;
          cout << t << " : " << maxi << endl;
      }
      delta.setZero();
      for (TUNGraph::TNodeI NI = Graph->BegNI(); NI < Graph->EndNI(); NI++) {
        delta.insert(0, NI.GetId()) = 1.0;
      }

      t += (t_2steps/2);
      P0 = Pt_before;
      
  }
  return t;

}

/**
 * Measures the mixing time of a graph using
 * Eigen library's Dense Matrix
 * @param  Graph   Undirected Graph
 * @param  epsilon 
 * @return         Mixing time
 */
double MixingTimeEigen(PUNGraph Graph, double epsilon){
  const int n = Graph->GetNodes();
  MatrixXd P(n, n);
  MatrixXd ds(1, n);
  MatrixXd delta = MatrixXd::Constant(1, n, 1.0);

  for (TUNGraph::TEdgeI EI = Graph->BegEI(); EI < Graph->EndEI(); EI++) {
    const TUNGraph::TNodeI srcNI = Graph->GetNI(EI.GetSrcNId());
    const TUNGraph::TNodeI dstNI = Graph->GetNI(EI.GetDstNId());
    
    P(srcNI.GetId(), dstNI.GetId()) = 1.0/(srcNI.GetOutDeg());
    P(dstNI.GetId(), srcNI.GetId()) = 1.0/(dstNI.GetOutDeg());
  }

  // Stationary Distribution
  for (TUNGraph::TNodeI NI = Graph->BegNI(); NI < Graph->EndNI(); NI++) {
    ds(0, NI.GetId()) = NI.GetOutDeg()/((double)2.0*Graph->GetEdges());
  }

  int t = 1;
  int t_2steps = 0;
  MatrixXd Pt, Pt_before, P0, Psqr;
  Pt = Pt_before = P0 = Psqr = P;
  while(t_2steps != 1){
      
      double maxi = 10;
      t_2steps = 1;
      Psqr = P;
      Pt_before = Pt;
      Pt = P0 * Psqr;
      double max_dist = 0;
      #pragma omp parallel for
      for (int i = 0; i < n; ++i)
      {
          double dist = (Pt.row(i) - ds).array().abs().sum()/2;
          delta(0, i) = dist;
          if(delta(0, i) > max_dist){
              max_dist = delta(0, i);
          }
      }
      maxi = max_dist;
      while(maxi > epsilon){
          Psqr = Psqr * Psqr;
          t_2steps*=2;
          Pt_before = Pt;
          Pt = P0 * Psqr;
          double max_dist = 0;
          #pragma omp parallel for
          for (int i = 0; i < n; ++i)
          {
              double dist = (Pt.row(i) - ds).array().abs().sum()/2;
              delta(0, i) = dist;
              
              if(delta(0, i) > max_dist){
                  max_dist = delta(0, i);
              }
          }
          maxi = max_dist;
          cout << t << " : " << maxi << endl;
      }
      delta = MatrixXd::Constant(1, n, 1.0);
      t += (t_2steps/2);
      P0 = Pt_before;
      
  }
  return t;

}

/**
 * Measures the mixing time of a graph using boost library's
 * @todo I haven't finished it yet because boost matrix operation is too slow
 * @param  Graph   Undirected Graph
 * @param  epsilon 
 * @return         Mixing time
 */
double MixingTimeBoost(PUNGraph Graph, double epsilon){
  const int n = Graph->GetNodes();
  ublas::compressed_matrix<double> C(n, n);
  ublas::compressed_matrix<double> P(n, n);
  ublas::compressed_matrix<double> ds(1, n);

  for (TUNGraph::TEdgeI EI = Graph->BegEI(); EI < Graph->EndEI(); EI++) {
    const TUNGraph::TNodeI srcNI = Graph->GetNI(EI.GetSrcNId());
    const TUNGraph::TNodeI dstNI = Graph->GetNI(EI.GetDstNId());
    
    P(srcNI.GetId(), dstNI.GetId()) = 1.0/(srcNI.GetOutDeg());
    P(dstNI.GetId(), srcNI.GetId()) = 1.0/(dstNI.GetOutDeg());
  }

  // Stationary Distribution
  for (TUNGraph::TNodeI NI = Graph->BegNI(); NI < Graph->EndNI(); NI++) {
    ds(0, NI.GetId()) = NI.GetOutDeg()/((double)2.0*Graph->GetEdges());  
  }

  C = ublas::prod(P, P);

  for (int i = 0; i < 503; ++i)
  {   
    C = ublas::prod(C, P);
    cout << "Non-zero : " << C.nnz() << endl;
  }

  return 0;
}

/**
 * Measures the mixing time os a graph using
 * dlib library
 * @param  Graph   Undirected Graph
 * @param  epsilon 
 * @return         Mixing time
 */
double MixingTime(PUNGraph Graph, double epsilon){
  int n = Graph->GetNodes();
  matrix<double> P = zeros_matrix<double>(n,n);
  matrix<double> ds(1, n);
  matrix<double> delta = ones_matrix<double>(1, n);
  
  for (TUNGraph::TEdgeI EI = Graph->BegEI(); EI < Graph->EndEI(); EI++) {
    const TUNGraph::TNodeI srcNI = Graph->GetNI(EI.GetSrcNId());
    const TUNGraph::TNodeI dstNI = Graph->GetNI(EI.GetDstNId());
    
    P(srcNI.GetId(), dstNI.GetId()) = 1.0/(srcNI.GetOutDeg());
    P(dstNI.GetId(), srcNI.GetId()) = 1.0/(dstNI.GetOutDeg());
  }

  // Stationary Distribution
  for (TUNGraph::TNodeI NI = Graph->BegNI(); NI < Graph->EndNI(); NI++) {
    ds(0, NI.GetId()) = NI.GetOutDeg()/((double)2.0*Graph->GetEdges());  
  }
  

  /*for (int i = 0; i < n; ++i)
  {
      IGraph::vertex * u = &vertices[i];
      ds(0, u->id) = u->outdegree/((double)numEdges());
      for (int j = 0; j < n; ++j)
      {
          IGraph::vertex * v = vertices[i].adj[j].second;
          if(v != NULL)
              P(u->id, v->id) = 1.0/(u->outdegree);
      }
  }*/


  /*matrix<double> eigenvector = real_eigenvalues(P);
  std::sort(&eigenvector(0), &eigenvector(0)+eigenvector.size());
  double mi = max(abs(eigenvector(1)), abs(eigenvector(eigenvector.size()-2)));
  cout << "MI : " << mi << endl;
  cout << "upper bound : " << (log(n) + log(1/epsilon))/(1-mi) << endl;
  cout << "lower bound : " << (mi/(2-2*mi))*log(1/(2*epsilon)) << endl;*/
  
  int t = 1;
  int t_2steps = 0;
  matrix<double> Pt, Pt_before, P0, Psqr;
  Pt = Pt_before = P0 = Psqr = P;
  while(t_2steps != 1){
      
      double maxi = 10;
      t_2steps = 1;
      Psqr = P;
      Pt_before = Pt;
      Pt = P0 * Psqr;
      double max_dist = 0;
      #pragma omp parallel for
      for (int i = 0; i < n; ++i)
      {
          double dist = sum(abs(rowm(Pt, i) - ds))/2;
          delta(0, i) = dist;
          
          if(delta(0, i) > max_dist){
              max_dist = delta(0, i);
          }
      }
      maxi = max_dist;
      while(maxi > epsilon){
          Psqr = Psqr * Psqr;
          t_2steps*=2;
          Pt_before = Pt;
          Pt = P0 * Psqr;
          double max_dist = 0;
          #pragma omp parallel for
          for (int i = 0; i < n; ++i)
          {
              double dist = sum(abs(rowm(Pt, i) - ds))/2;
              delta(0, i) = dist;
              
              if(delta(0, i) > max_dist){
                  max_dist = delta(0, i);
              }
          }
          maxi = max_dist;
          cout << t << " : " << maxi << endl;
      }
      delta = ones_matrix<double>(1, n); 
      t += (t_2steps/2);
      P0 = Pt_before;
      
  }

  return t;
}

/**
 * Main function
 * @param  argc
 * @param  argv
 * @return
 */
int main(int argc, char* argv[]) {
  Env = TEnv(argc, argv, TNotify::StdNotify);
  Env.PrepArgs(TStr::Fmt("Mixing time measure. build: %s, %s. Time: %s", __TIME__, __DATE__, TExeTm::GetCurTm()));
  TExeTm ExeTm;
  Try
    const TStr InFNm = Env.GetIfArgPrefixStr("-i:", "graph.txt", "Input graph (undirected graph)");
  
    PUNGraph Graph = TSnap::LoadEdgeList<PUNGraph>(InFNm, false);
    //PUNGraph Graph = TSnap::LoadEdgeList<PUNGraph>("../as20graph.txt", false);
    //PUNGraph Graph = TSnap::GenRndGnm<PUNGraph>(5000, 10000); // generate a random graph

    TSnap::DelSelfEdges(Graph);
    
    cout << "Mixing time : " << MixingTimeEigen(Graph, 1.0/(double)Graph->GetNodes()) << endl;

  Catch
    printf("\nrun time: %s (%s)\n", ExeTm.GetTmStr(), TSecTm::GetCurTm().GetTmStr().CStr());
  
  return 0;
}
